let loginForm = document.querySelector("#logInUser");


//add an event listener
//(e) - when it is triggered, it will create an event object
loginForm = addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	console.log(email);
	console.log(password);

	if(email == "" || password == ""){
		alert("Please input your email and/or password.")
	} else {
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data.accessToken){
				//store JWT in local storage
				//localStorage - inistore sa console log ng isang webpage
				localStorage.setItem("token", data.accessToken);
				//send fetch request to decode JWT and obtain user ID & role for storing in context
				//backtick dahil token ang kukunin
				fetch('http://localhost:4000/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					//set global user to have properties containing authenticated user's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("asAdmin", data.isAdmin)
					window.location.replace("./courses.html")
				})
			}else {
				//authentication failed
				alert("Something went wrong!")
			}
		})
	}
})