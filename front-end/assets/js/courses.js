let adminUser = localStorage.getItem("isAdmin");

//cardfooter will be dynamically rendered if the user is an admin or not
let cardFooter;

//fetch request to all the available user
fetch('http://localhost:4000/api/courses')
.then(res => res.json())
.then(data => {
	//log the data to check if you were able to fetch the data from the server
	console.log(data);

	//courseData will store the data to be rendered
	let courseData

	if(data.length < 1) {
		courseData = "No courses available."
	} else {
		//else iterate the course collection and display each course
		//check the make up of each element inside the courses collection
		courseData = data.map(course => {
			console.log(course._id)

			//if the user is regular user, display the course was created
			if(!adminUser){
				cardFooter =
				`
					<a href="./course.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block editButton">
						Select Course
					</a>
				`
			}else {
				cardFooter = 
				`
					<a href="./editCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block editButton">Edit</a>
					<a href="./deleteCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block dangerButton">Delete</a>
				`
			}
			return(
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
								${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`
				)
		}).join("");
	}//get the value of the courseData and assign it as a #courseContainer's content
	let container = document.querySelector("#coursesContainer")
	container.innerHTML = courseData;
})

let modalButton = document.querySelector("#adminButton")

if(adminUser == "false || !adminUser") {
	modalButton.innerHTML = null
} else {
	modalButton.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse class="btn btn-block btn-primary">Add Course</a>
	</div>
	`
}